module.exports = {
  css: {
    sourceMap: true
  },
  devServer: {
    open: true,
    port: 8085
  }
}
