import Vue from 'vue'
import Router from 'vue-router'
import Contacts from './views/Contacts.vue'
import Error404 from './views/Error404.vue'
import Error503 from './views/Error503.vue'
import Events from './views/Events.vue'
import Event from './views/Event.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior () {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Events
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: Contacts
    },
    {
      path: '/404',
      name: 'Error404',
      component: Error404
    },
    {
      path: '/503',
      name: 'Error503',
      component: Error503
    },
    {
      path: '/:id',
      name: 'Event',
      component: Event
    }]
})
