import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import cookies from 'js-cookie'

const isProd = process.env.NODE_ENV === 'production'

Vue.use(Vuex)

const maximumCityList = [
  { name: 'Владимир', okato: '17' },
  { name: 'Волгоград', okato: '18' },
  { name: 'Грозный', okato: '96' },
  { name: 'Екатеринбург', okato: '65' },
  { name: 'Иркутск', okato: '25' },
  { name: 'Краснодар', okato: ['3', '03'] },
  { name: 'Красноярск', okato: '40' },
  { name: 'Москва', okato: ['46', '45'] },
  { name: 'Мытищи', okato: '46234501' },
  { name: 'Нижний Новгород', okato: '49' },
  { name: 'Новосибирск', okato: '50' },
  { name: 'Одинцово', okato: '46241501' },
  { name: 'Онлайн', okato: '0' },
  { name: 'Подольск', okato: '4646' },
  { name: 'Ростов-на-Дону', okato: '60401' },
  { name: 'Санкт-Петербург', okato: '41' },
  { name: 'Тольятти', okato: '36' },
  { name: 'Тула', okato: '70' },
  { name: 'Сургут', okato: '71876000001' },
  { name: 'Ярославль', okato: '78' }
]

const host = process.env.NODE_ENV === 'production' ? '' : 'http://localhost:8089'

const store = new Vuex.Store({
  state: {
    city: { _id: 0, city: 'Онлайн' },
    cityList: [{ _id: 0, city: 'Онлайн' }],
    registration: {
      success: false
    },
    events: [],
    maximumCityList,
    cityFromCookie: false,
    cityFromServer: '',
    event: {
      onlineEventWindow: {
        title: '',
        blocks: []
      },
      theses: [],
      date: new Date()
    },
    isMobile: window.matchMedia('(max-width: 980px)').matches
  },
  mutations: {
    changeCity (state, city) {
      const fromList = maximumCityList.find(el => el.name === city.city)
      if (fromList) {
        cookies.set('city-okato', Array.isArray(fromList.okato) ? fromList.okato[0] : fromList.okato, isProd ? {
          domain: '.maximumtest.ru'
        } : {})
        state.city = city
      } else if (city.city === 'Онлайн') {
        state.city = city
      }
    },
    setCityList (state, list) {
      state.cityList = [{ _id: 0, city: 'Онлайн' }].concat(list).sort((a, b) => {
        if (a.city > b.city) {
          return 1
        }
        if (a.city < b.city) {
          return -1
        }
        return 0
      })
      checkCookieCity()
    },
    setRegistrationSuccess (state, value) {
      state.registration.success = value
    },
    setEvents (state, payload) {
      state.events = payload
    },
    setEvent (state, payload) {
      state.event = payload
    },
    cityFromCookie (state, payload) {
      state.cityFromCookie = payload
    },
    setCityFromServer (state, payload) {
      state.cityFromServer = payload
    },
    resetRegistrationState (state) {
      state.registration.success = false
    },
    setMobile (state) {
      state.isMobile = window.matchMedia('(max-width: 980px)').matches
    }
  },
  actions: {
    async getCityList ({ commit }) {
      const { data } = await axios.get(`${host}/api/getCityList`)
      commit('setCityList', data)
      return data
    },
    async registration ({ commit }, payload) {
      const { data } = await axios.post(`${host}/api/registration`, payload)
      commit('setRegistrationSuccess', data.success)
    },
    async getEvents ({ commit }, payload) {
      const { data } = await axios.get(`${host}/api/getEvents`, {
        params: payload
      })
      commit('setEvents', data)
    },
    async getCity ({ commit }, change = true) {
      const { data } = await axios.get(`${host}/api/getCity`)
      if (change) {
        const fromList = maximumCityList.find(el => el.name === data.city)
        if (fromList) {
          commit('changeCity', data)
        } else {
          commit('changeCity', { city: 'Онлайн', okato: '0', _id: 0 })
        }
      }
      commit('setCityFromServer', data.city)
    },
    async getEvent ({ commit }, payload) {
      const { data } = await axios.get(`${host}/api/getEvent`, {
        params: payload
      })
      commit('setEvent', data)
    }
  }
})

const checkCookieCity = () => {
  const { state, commit, dispatch } = store
  const cookieCity = cookies.get('city-okato')
  let findedCity
  if (cookieCity !== undefined) {
    findedCity = maximumCityList.find(el => Array.isArray(el.okato) ? el.okato.includes(cookieCity + '') : el.okato === cookieCity + '')
    if (findedCity) {
      const city = state.cityList.find(el => el.city === findedCity.name)
      if (city) {
        commit('changeCity', {
          ...city,
          okato: findedCity.okato
        })
        commit('cityFromCookie', true)
        if (city.city === 'Онлайн') {
          dispatch('getCity', false)
        }
      } else {
        dispatch('getCity')
      }
    } else {
      dispatch('getCity')
    }
  } else {
    dispatch('getCity')
  }
}

window.onresize = () => {
  store.commit('setMobile')
}

export default store
