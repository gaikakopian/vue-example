import Vue from 'vue'
import VS2 from 'vue-script2'
import filter from 'vue-bulma-emoji/src/filter'
import '@/tools/Locals.js'

import App from './App.vue'
import router from './router'
import store from './store'
import jQuery from 'jquery'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueYouTubeEmbed from 'vue-youtube-embed'

window.jQuery = jQuery

Vue.config.productionTip = false
Vue.use(VueAwesomeSwiper)
Vue.use(VS2)
Vue.use(VueYouTubeEmbed, { global: true, componentId: 'youtube' })
Vue.use(filter)

store.dispatch('getCityList')

document.title = 'Мероприятия'

store.dispatch('getCityList').then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})
